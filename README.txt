========================================================
Kiaro Networking Module Test Suite v0.2.0.0

This software is licensed under the GNU Lesser General Public
License version 3. Please refer to gpl.txt and lgpl.txt for further
information.

Copyright (c) 2013 Draconic Entertainment
========================================================

Table of Contents:
	I. Welcome
	II. Prerequisites
	III. Compiling the Engine
	IV. Compiling the Test Suites
	VI. Warning

I. WELCOME
	Kiaro Game Engine is an FPS engine designed to be as modular as
possible in the sense that any third party libraries are coded into external
code files (.DLL, .SO, etc) known as "modules" which allow for the swapping,
upgrading and custom rewriting of all internal components built to work
on this system. This makes the engine effective on even older hardware
since the renderer, for example, can be swapped out for a low-end one
therefore decreasing system load while keeping all other functionality 
intact so that compatability is not harmed either.

II. PREREQUISITES
	1.) A cmd-line accessible Python 2.7 binary. You should be able
to run "python" in your terminal and get the Python interactive
scripting environment. Certain build scripts are written in Python.
	Ubuntu (should be installed): sudo apt-get install python
	Windows / OSX: http://www.python.org/download/

	2.) Source code to Python 2.7. Python is used as the interal
scripting engine of Kiaro and is therefore a requirement.
	Ubuntu: sudo apt-get install python-dev
	Windows / OSX: http://www.python.org/download/

	3.) Source code to the Bullet Physics engine. Bullet is the 
current physics engine built for Kiaro by default.
	Ubuntu: sudo apt-get install libbullet-dev
	Windows / OSX: http://code.google.com/p/bullet/downloads/list

	4.) Libraries for the irrKlang -OR- FMod audio engines. Both of these
audio libraries are available for noncommercial use but must be paid-for
in order to be used in any type of project that results in profit for some
holding party.
	F-Mod: http://www.fmod.org/fmod-downloads.html
	irrKlang:  http://www.ambiera.com/irrklang/downloads.html

	Be sure to set your search directories to the include folders of either
software package and be sure to link against the proper libraries provided
by either.

	The preprocessor definition _KIARO_AUDIO_USE_FMOD_ should
not be defined if you are wanting to build the Kiaro audio module with
irrKlang rather than FMod.

	5.) Libraries for Irrlicht. Irrlicht is the current renderer implemented
by Kiaro's rendering module.

	Be sure to set your search directories to the include folders of Irrlicht
and be sure to link against the proper Irrlicht libraries.

III. COMPILING THE ENGINE
	As of this moment, compiling the Kiaro Game Engine requires a copy
of Code::Blocks and an Ubuntu Linux machine. Even though there are
Windows and OSX targets listed, they are currently in development. Only
the Linux x86 and x64 targets work as intended.

	1.) Navigate to the engine/projects/codeblocks folder.
	2.) Open "KGE.workspace" with Code::Blocks.
	3.) Select your Build Target. (Either Debug Linux x64/x86 or Release Linux x64/x86) 
	4.) Several projects will appear in the list to the left. Perform a full rebuild
of every listed project, leaving Kiaro Application to be last as it depends on
all of the other projects to be built first.
	5.) Navigate to bin/ folder and run your newly built application. You may package up
the bin folder for distribution.

IV. COMPILING THE TEST SUITES
	The process of compiling the module test suites is nearly the same as above, however 
you must navigate to a specific project under engine/suites/projects/codeblocks and build
it from the Code::Blocks project. You must be sure the proper Kiaro module is built for your
test suite and target. If you're trying to compile the networking test suite for Linux x64
then the Linux x64 target for the networking module better be built otherwise linker
errors will occur.

VI. WARNING
	If this software repository had been cloned directly from Bitbucket,
it is highly recommended you download the full SDK package from the 
Bitbucket under the Kiaro Application code repository. The full SDK
package comes with the project files required to properly build this
software.