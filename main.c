/*
    main.c

    Kiaro Rendering test suite.

    This software is licensed under the GNU Lesser General
    Public License version 3. Please refer to gpl.txt and
    lgpl.txt for more information.

    Copyright (c) 2013 Draconic Entertainment
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <KiaroRendering.h>

int main(int argc, char *argv[])
{
    // Allocate memory
    unsigned int real_size = sizeof(KiaroRenderingContext);
    KiaroRenderingContext *context = (KiaroRenderingContext*)malloc(real_size);
    if (!context)
    {
        printf("FATAL: Unable to allocate memory for the Kiaro rendering context!\n");
        return 1;
    }
    memset(context, 0x00, real_size);

    KiaroRenderingInitialize(context, KIARO_RENDERING_DRIVER_OPENGL, 640, 480, 1, 0, 0, 0, 0);
    KiaroRenderingSetWindowTitle("Kiaro Rendering Test Suite");

    char title_string[256];
    while (KiaroRenderingActive())
    {
        sprintf(title_string, "Kiaro Rendering Test Suite [FPS: %u]", KiaroRenderingGetFramesPerSecond());
        KiaroRenderingSetWindowTitle(title_string);
        KiaroRenderingUpdate();
    }

    // This also frees our memory pointer to the KiaroRenderingContext for us
    KiaroRenderingDeinitialize();
    return 0;
}
